# -*- coding: utf-8 -*-


import scrapy
from scrapy import Request
 
from tutorial.items import MasterSeekItem


class DmozSpider(scrapy.Spider):
    name = "masterseek"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
    }

    def start_requests(self):
        url = 'http://www.masterseek.com/V15.0/AjaxPage.aspx?requestid=1&country=&keyword=Whiskey%20Glass&npage=40'
        yield Request(url, headers=self.headers)

    def parse(self, response):
        # from scrapy.shell import inspect_response
        # inspect_response(response, self)
        urls = response.xpath('/html/body/li/div[3]/h2')
        for url in urls:
            item = MasterSeekItem()
            item['name'] = url.xpath('./text()').extract()[0]
            # item['eng_name'] = url.xpath('./div/div[2]/div[1]/a/span[2]/text()').extract()[0]
            # item['year'] = url.xpath('./div/div[2]/div[2]/p[1]/text()[2]').extract()[0]
            yield item

