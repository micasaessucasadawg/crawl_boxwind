# -*- coding: utf-8 -*-
import re

import scrapy

from tutorial import settings
from tutorial.items import BoxWindItem


class DmozSpider(scrapy.Spider):
    # init searching page count
    def __init__(self):
        self.now_search_page_count = 0

    search_key = settings.BOX_WIND_SEARCH_KEY
    search_page_count = settings.COUNT
    name = "boxwind"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
    }

    def start_requests(self):
        url = 'http://www.boxwind.com/q.html'
        yield scrapy.FormRequest(
            url=url,
            formdata={"m": "s", "k": self.search_key},
            headers=self.headers,
            callback=self.parse
        )

    def parse(self, response):
        # from scrapy.shell import inspect_response
        # inspect_response(response, self)
        urls = response.xpath('//*[@class="col-md-12 column"]/dl/div[4]/a/@href')
        next_pages_text = response.xpath("//*[@class='pagination']/li/a/text()").extract()
        next_page = response.xpath("//*[@class='pagination']/li/a/@href").extract()

        for url in urls:
            print url
            request_url = url.extract().encode("utf-8")
            print request_url
            yield scrapy.Request(request_url, callback=self.parse_page)

        if u'Next' in next_pages_text and self.now_search_page_count < self.search_page_count:
            next_url = "http://www.boxwind.com{}".format(next_page[-1])
            print 'next_url: ' + next_url
            self.now_search_page_count += 1
            yield scrapy.Request(next_url, headers=self.headers, callback=self.parse)

    def parse_page(self, response):
        # from scrapy.shell import inspect_response
        # inspect_response(response, self)
        items = BoxWindItem()

        name_raw = response.xpath('/html/body/div[2]/div/div/div[2]/div[1]/div[1]/h1/text()').extract()[0]
        # trim item name
        pattern = re.compile(r'[\t\n]+')
        name = re.sub(pattern, '', name_raw)
        items['name'] = name.encode('utf-8')

        url = response.xpath('/html/body/div[2]/div/div/div[2]/div[1]/div[7]/div[2]/strong/text()').extract()[0]
        items['url'] = url

        body = response.body

        # using regular expression match emails
        match_email = re.search(r'[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+', body)
        if match_email:
            email = match_email.group()
            print email
            items['email'] = email
        yield items
